//
//  DataModel.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/9/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import Foundation
import CoreData

class Node {  }

class DataController
{
    class func loadData(){
        let mainBundle = Bundle.main
        let url = mainBundle.url(forResource: "trees-db", withExtension: "sqlite")!
        
        var db: OpaquePointer? = nil
        if sqlite3_open(url.path, &db) != SQLITE_OK {
            print("Load trees data failed: error opening database")
        }
        defer {
            if sqlite3_close(db) != SQLITE_OK {
                print("error closing database")
            }
            db = nil
        }
        
        TreeInfo.loadDataFromSQLite(db: db)
        Criteria.loadDataFromSQLite(db: db)
    }
    
    class func getIdFromSQLiteTextColumn(_ statement: OpaquePointer, columnIndex: Int32) -> String {
        let id = sqlite3_column_int64(statement, columnIndex)
        let idStr = String(id)
        return idStr
    }
    
    class func getStringFromSQLiteTextColumn(_ statement: OpaquePointer, columnIndex: Int32) -> String? {
        let address = sqlite3_column_text(statement, columnIndex)
        let result = address.map({ addr in String(cString: addr) }) ?? nil
        return result
    }
    
    class func getIntFromSQLiteTextColumn(_ statement: OpaquePointer, columnIndex: Int32) -> Int {
        let result = sqlite3_column_int(statement, columnIndex)
        return Int(result)
    }
    
    class func getDoubleFromSQLiteTextColumn(_ statement: OpaquePointer, columnIndex: Int32) -> Double {
        let result = sqlite3_column_double(statement, columnIndex)
        return Double(result)
    }
}

class TreeInfo {
    
    static var tagToTreeInfo : Dictionary<String,TreeInfo>?
    static var idToTreeInfo : Dictionary<Int,TreeInfo>?
    
    let id : Int, tag : String, title : String, subtitle : String, content : String, imageName : String
    init(id : Int, tag : String, title : String, subtitle : String, content : String, imageName : String){
        self.id = id
        self.tag = tag
        self.imageName = imageName
        self.title = title
        self.subtitle = subtitle
        self.content = content
    }
    
    class func getAllTreeInfo() -> [TreeInfo]? {
        return idToTreeInfo?.values.map({$0})
    }
    
    class func loadDataFromSQLite(db: OpaquePointer?) {
        var statement: OpaquePointer? = nil
        let sqlQuery = "select id, tag, title, subTitle, description, imageName from trees"
        if sqlite3_prepare_v2(db, sqlQuery, -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        var trees: [TreeInfo] = []
        while sqlite3_step(statement) == SQLITE_ROW {
            let tree = loadTreeFromSQLite(statement!)
            trees.append(tree)
        }
        
        self.idToTreeInfo = trees.toDictionary({ $0.id }, valueSelector: { $0 })
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        statement = nil
    }
    
    class func loadTreeFromSQLite(_ statement: OpaquePointer) -> TreeInfo {
        var idxVal : Int32 = 0
        let colIdx : () -> Int32 = { let oldVal = idxVal; idxVal += 1; return oldVal; }
        let id = DataController.getIntFromSQLiteTextColumn(statement, columnIndex: colIdx())
        let tag = DataController.getStringFromSQLiteTextColumn(statement, columnIndex: colIdx())!
        let title = DataController.getStringFromSQLiteTextColumn(statement, columnIndex: colIdx())!
        let subTitle = DataController.getStringFromSQLiteTextColumn(statement, columnIndex: colIdx())!
        let description = DataController.getStringFromSQLiteTextColumn(statement, columnIndex: colIdx())!
        let imageName = DataController.getStringFromSQLiteTextColumn(statement, columnIndex: colIdx())!
        let result = TreeInfo(id: id, tag: tag, title: title, subtitle: subTitle, content: description, imageName: imageName)
        return result
        
    }
    
    class func getTreeInfoByTag(tag:String) -> TreeInfo {
        //todo: ...
        return tagToTreeInfo?[tag] ?? TreeInfo(id: 0, tag: tag, title: "Unknown result", subtitle: "", content: "No Content", imageName: "test_image")
    }
    
    class func getTreeInfoById(id:Int) -> TreeInfo {
        //todo: ...
        return idToTreeInfo?[id] ?? TreeInfo(id: id, tag: "", title: "Unknown result", subtitle: "", content: "No Content", imageName: "test_image")
    }
    
    class func getTreeInfosByCriteria(criteria:Criteria) -> [TreeInfo] {
        let result = Set(criteria.getCriteriaResults())
            .flatMap({ getTreeInfoById(id: $0.treeId) })
        return result
    }
}

class Criteria : Node {
    
    static var rootCriteria : Criteria?
    
    var name : String
    var possibleValues : [CriteriaOption]
    init(name : String, possibleValues : [CriteriaOption]){
        self.name = name
        self.possibleValues = possibleValues
    }
    
    class func loadDataFromSQLite(db: OpaquePointer?) {
        let criteriaOptionRecords = loadCriteriaOptionsDataFromSQLite(db:db)
        let criteriaRecords = loadCriteriaDataFromSQLite(db: db)
        
        let criteriaOptionResults = criteriaOptionRecords
            .filter({ $0.4 })
            .map({ CriteriaResult(treeId: $0.3) })
            .toDictionary({ OptionKey(id: $0.treeId, isTree: true) }, valueSelector: { $0 as Node })
        
        let rootCriteriaRecord = getRootCriteria(criteriaRecords:criteriaRecords, criteriaOptionRecords:criteriaOptionRecords)
        let idToCriteriaRecord = criteriaRecords.toDictionary({ $0.0 }, valueSelector: { $0 })
        let criteriaOptionRecordsByCriteriaId = criteriaOptionRecords.groupBy({ $0.0 })
        
        self.rootCriteria = createCriteria(criteriaRecord: rootCriteriaRecord, idToCriteriaRecord: idToCriteriaRecord,
                       criteriaOptionRecords: criteriaOptionRecordsByCriteriaId,
                       idToNode: criteriaOptionResults).0
    }
    
    class func getRootCriteria(criteriaRecords:[(Int, String)],
                               criteriaOptionRecords: [(Int, String, String, Int, Bool)]) -> (Int, String) {
        let linkIdToCriteriaOption = criteriaOptionRecords
            .filter({ !$0.4 })
            .groupBy({ $0.3 })
        let result = criteriaRecords.filter({ linkIdToCriteriaOption[$0.0] == nil }).first!
        return result
    }
    
    class func createCriteria(
        criteriaRecord: (Int, String),
        idToCriteriaRecord: Dictionary<Int, (Int, String)>,
        criteriaOptionRecords: Dictionary<Int, [(Int, String, String, Int, Bool)]>,
        idToNode: Dictionary<OptionKey,Node>) -> (Criteria, Dictionary<OptionKey,Node>) {
        
        let possibleValuesRecords = criteriaOptionRecords[criteriaRecord.0]
        let possibleValuesIdToNode = possibleValuesRecords!
            .reduce((idToNode, [] as [CriteriaOption]), { acc, r in
                let optionKey = OptionKey(id: r.3, isTree: r.4)
                var node = idToNode[optionKey]
                var map = acc.0
                switch node{
                case .none:
                    let nodeMap = createCriteria(criteriaRecord: idToCriteriaRecord[r.3]!,
                                   idToCriteriaRecord: idToCriteriaRecord,
                                   criteriaOptionRecords: criteriaOptionRecords,
                                   idToNode: map)
                    node = nodeMap.0
                    map = nodeMap.1
                    map[optionKey] = node
                    break
                default:
                    break
                }
                let accr = acc.1 + [CriteriaOption(title: r.1, subTitle: r.2, nextStep: node!)]
                return (map, accr)
            })
        let possibleValues = possibleValuesIdToNode.1
        return (Criteria(name: criteriaRecord.1, possibleValues: possibleValues), possibleValuesIdToNode.0)
    }
    
    func getCriteriaResults() -> [CriteriaResult] {
        let subCriterias  = self.possibleValues
            .flatMap({ $0.nextStep as? Criteria })
            .flatMap({ $0.getCriteriaResults() })
        let results = self.possibleValues
            .flatMap({ $0.nextStep as? CriteriaResult })
        return results + subCriterias
    }
    
    class func loadCriteriaOptionsDataFromSQLite(db: OpaquePointer?) -> [(Int, String, String, Int, Bool)] {
        var statement: OpaquePointer? = nil
        let sqlQuery = "select criteriaId, title, subtitle, linkId, linkType from criteriaOption"
        if sqlite3_prepare_v2(db, sqlQuery, -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        var result: [(Int, String, String, Int, Bool)] = []
        while sqlite3_step(statement) == SQLITE_ROW {
            let criteraOption = loadCriteriaOptionFromSQLite(statement!)
            result.append(criteraOption)
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        statement = nil
        return result
    }
    
    class func loadCriteriaDataFromSQLite(db: OpaquePointer?) -> [(Int, String)] {
        var statement: OpaquePointer? = nil
        let sqlQuery = "select id, title from criteria"
        if sqlite3_prepare_v2(db, sqlQuery, -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        var result: [(Int, String)] = []
        while sqlite3_step(statement) == SQLITE_ROW {
            let critera = loadCriteriaFromSQLite(statement!)
            result.append(critera)
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        statement = nil
        return result
    }
    
    class func loadCriteriaOptionFromSQLite(_ statement: OpaquePointer) -> (Int, String, String, Int, Bool) {
        var idxVal : Int32 = 0
        let colIdx : () -> Int32 = { let oldVal = idxVal; idxVal += 1; return oldVal; }
        let criteriaId = DataController.getIntFromSQLiteTextColumn(statement, columnIndex: colIdx())
        let title = DataController.getStringFromSQLiteTextColumn(statement, columnIndex: colIdx())!
        let subTitle = DataController.getStringFromSQLiteTextColumn(statement, columnIndex: colIdx())!
        let linkId = DataController.getIntFromSQLiteTextColumn(statement, columnIndex: colIdx())
        let isTreeResult = DataController.getIntFromSQLiteTextColumn(statement, columnIndex: colIdx()) == 1
        let result = (criteriaId, title, subTitle, linkId, isTreeResult)
        return result
    }
    
    class func loadCriteriaFromSQLite(_ statement: OpaquePointer) -> (Int, String) {
        var idxVal : Int32 = 0
        let colIdx : () -> Int32 = { let oldVal = idxVal; idxVal += 1; return oldVal; }
        let criteriaId = DataController.getIntFromSQLiteTextColumn(statement, columnIndex: colIdx())
        let title = DataController.getStringFromSQLiteTextColumn(statement, columnIndex: colIdx())!
        let result = (criteriaId, title)
        return result
    }
    
    class func loadDataFromSQLite(
        criteria: (Int, String),
        criteriaOption: [(Int, Int, String, String, Int, Bool)],
        allCriterias: [(Int, String)]) -> Node? {
        return nil
    }
    
}

class CriteriaResult : Node, Hashable {
    var treeId : Int
    init(treeId: Int) {
        self.treeId = treeId
    }
    var hashValue: Int {
        return treeId.hashValue
    }
    static func == (lhs: CriteriaResult, rhs: CriteriaResult) -> Bool {
        return lhs.treeId == rhs.treeId
    }
}

class CriteriaOption
{
    let nextStep: Node
    let title: String
    let subTitle: String
    
    init(title: String, subTitle: String, nextStep: Node){
        self.title = title
        self.subTitle = subTitle
        self.nextStep = nextStep
    }
}

class OptionKey : Hashable {
    let isTree: Bool
    let id: Int
    init(id: Int, isTree: Bool) {
        self.id = id
        self.isTree = isTree
    }
    
    var hashValue: Int {
        return id.hashValue * isTree.hashValue
    }
    
    static func == (lhs: OptionKey, rhs: OptionKey) -> Bool {
        return lhs.id == rhs.id && lhs.isTree == rhs.isTree
    }
}
