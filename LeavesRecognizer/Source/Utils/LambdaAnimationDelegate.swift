//
//  LambdaAnimationDelegate.swift
//  Bulgarian National & Natural Parks
//
//  Created by Nickolay Kunev on 8.07.16 г..
//  Copyright © 2016 г. Divine Bulgaria Ltd. All rights reserved.
//

import Foundation
import QuartzCore

open class LambdaAnimationDelegate : NSObject, CAAnimationDelegate {
    
    var coreAnimationDidStartDelegate : ((CAAnimation)->())!
    var coreAnimationDidEndDelegate : ((CAAnimation,Bool)->())!
    
    init(animationDidStart:((CAAnimation)->())!, animationDidEnd:((CAAnimation,Bool)->())!){
        self.coreAnimationDidStartDelegate = animationDidStart
        self.coreAnimationDidEndDelegate = animationDidEnd
        super.init()
    }
    
    open func animationDidStart(_ anim: CAAnimation){
        if self.coreAnimationDidStartDelegate != nil {
            self.coreAnimationDidStartDelegate(anim)
        }
    }
     
    open func animationDidStop(_ anim: CAAnimation, finished: Bool){
        if self.coreAnimationDidEndDelegate != nil {
            self.coreAnimationDidEndDelegate(anim,finished)
        }
    }
}
