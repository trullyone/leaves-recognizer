//
//  CollectionsHelper.swift
//  Bulgarian National & Natural Parks
//
//  Created by Nickolay Kunev on 16.08.16 г..
//  Copyright © 2016 г. Divine Bulgaria Ltd. All rights reserved.
//

import Foundation

extension Sequence {

    func toDictionary<K, V>(_ keySelector:(Iterator.Element) -> K, valueSelector:(Iterator.Element) -> V) -> Dictionary<K, V>
    {
        return self.reduce([:]) {
            (d, e) in
            var dict = d
            let key = keySelector(e), value = valueSelector(e)
            dict[key] = value
            return dict
        }
    }
    
    // Groups elements of self into a dictionary, with the keys given by keyFunc
    func groupBy<U : Hashable>(_ keyFunc: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var dict: [U:[Iterator.Element]] = [:]
        for el in self {
            let key = keyFunc(el)
            if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
    
    
    func any(predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> Bool {
        for element in self {
            let result = try predicate(element)
            if result {
                return true
            }
        }
        return false
    }
    
    
    func all(predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> Bool {
        for element in self {
            let result = try predicate(element)
            if !result {
                return false
            }
        }
        return true
    }
    
    
    func none(predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> Bool {
        for element in self {
            let result = try predicate(element)
            if result {
                return false
            }
        }
        return true
    }
}

struct Two<T:Hashable,U:Hashable> : Hashable {
    let values : (T, U)
    
    var hashValue : Int {
        get {
            let (a,b) = values
            return a.hashValue &* 31 &+ b.hashValue
        }
    }
}

// comparison function for conforming to Equatable protocol
func ==<T:Hashable,U:Hashable>(lhs: Two<T,U>, rhs: Two<T,U>) -> Bool {
    return lhs.values == rhs.values
}
