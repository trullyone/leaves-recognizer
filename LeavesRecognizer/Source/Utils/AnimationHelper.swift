//
//  AnimationHelper.swift
//  Bulgarian National & Natural Parks
//
//  Created by Nickolay Kunev on 12.07.16 г..
//  Copyright © 2016 г. Divine Bulgaria Ltd. All rights reserved.
//


import UIKit

struct AnimationHelper {
    
    static func perspectiveTransformForContainerView(_ containerView: UIView) {
        var transform = CATransform3DIdentity
        transform.m34 = -0.002
        containerView.layer.sublayerTransform = transform
    }
    
    static func updateAnchorPointAndOffset(_ anchorPoint: CGPoint, view: UIView){
        let oldAnchorPoint = view.layer.anchorPoint
        let xOffset =  anchorPoint.x - oldAnchorPoint.x
        let yOffset =  anchorPoint.y - oldAnchorPoint.y
        view.layer.anchorPoint = anchorPoint
        view.frame = view.frame.offsetBy(dx: xOffset * view.frame.size.width, dy: yOffset * view.frame.size.height)
    }
    
    static func moveViewOffScreen(_ view:UIView, containerView:UIView){
        let viewFrame = view.frame
        view.frame = view.convert(viewFrame.offsetBy(dx: -viewFrame.width, dy: -viewFrame.height), to: containerView)
    }
}
