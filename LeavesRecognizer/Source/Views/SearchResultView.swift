//
//  SearchResultView.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/9/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class SearchResultView: UIView {

    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblAccuracy : UILabel!
    
    func loadSearchResult(searchResult:(TreeInfo,Int)){
        self.lblAccuracy.text = String(searchResult.1) + "%"
        let treeInfo = searchResult.0
        self.lblTitle.text = treeInfo.title
        self.lblSubTitle.text = treeInfo.subtitle
    }
}
