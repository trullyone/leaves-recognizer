//
//  TitleView.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/7/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TitleView: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imvIcon: UIImageView!

    var icon : UIImage?{
        didSet{
            self.imvIcon?.image = icon
        }
    }
    
    var title : String?{
        didSet{
            self.lblTitle?.text = title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblTitle?.text = title
        self.imvIcon?.image = icon
    }
}
