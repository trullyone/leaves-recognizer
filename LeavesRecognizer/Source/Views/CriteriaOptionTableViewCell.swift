//
//  CriteriaOptionTableViewCell.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/12/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class CriteriaOptionTableViewCell: UITableViewCell {

    var title : String? {
        didSet{
            self.lblTitle?.text = title
        }
    }
    var subTitle : String?{
        didSet{
            self.lblSubtitle?.text = subTitle
        }
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblTitle?.text = title
        self.lblSubtitle?.text = subTitle
    }
}
