//
//  SavedToLibraryAnimationView.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/13/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class SavedToLibraryAnimationView: UIView {

    @IBOutlet weak var lblS: UILabel!
    @IBOutlet weak var lblA: UILabel!
    @IBOutlet weak var lblV: UILabel!
    @IBOutlet weak var lblE: UILabel!
    @IBOutlet weak var lblD: UILabel!
    
    var animationInProgress = false
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 35
    }
    
    func playAnimation(onComplete: @escaping () -> Void) {
        guard !animationInProgress else {
            return
        }
        animationInProgress = true
        self.alpha = 0
        self.notifficationsOverlayShapeLayer?.isHidden = true
        let label_index_s = [(lblS, 0), (lblA, 1), (lblV, 2), (lblE, 3), (lblD, 4)]
        for lbl_index in label_index_s {
            lbl_index.0?.transform = CGAffineTransform()
                .scaledBy(x: 0.1, y: 0.1)
            lbl_index.0?.alpha = 0
        }
        self.setNeedsLayout()
        self.layoutIfNeeded()
        UIView.animate(withDuration: 0.15, animations: {
            self.alpha = 1
        }, completion: { c in
            for lbl_index in label_index_s {
                self.playTickAnimation()
                UIView.animate(withDuration: 0.2, delay:  0.07 * Double(lbl_index.1), options: [], animations: {
                    lbl_index.0?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
                    lbl_index.0?.alpha = 0.35
                }, completion: { c in
                    UIView.animate(withDuration: 0.25, animations: {
                        lbl_index.0?.transform = CGAffineTransform(scaleX: 1, y: 1)
                        lbl_index.0?.alpha = 1
                    }, completion: { c in
                        guard lbl_index.1 == label_index_s.count - 1 else { return }
                        UIView.animate(withDuration: 2, animations: {
                            self.alpha = 0
                        }, completion: { c in
                            guard lbl_index.1 == label_index_s.count - 1 else { return }
                            onComplete()
                            self.animationInProgress = false
                        })
                    })
                })
            }
        })
    }
    
    func drawTick(){
        //Todo:
        let shapeLayer = self.notifficationsOverlayShapeLayer!
        //path scale properties
        let overlayWidth = self.bounds.width
        let heartShapeWidth = 0.5 * overlayWidth
        let scale : CGFloat = heartShapeWidth / 200
        let startPoint = CGPoint(x: self.bounds.midX - 35, y: self.bounds.midY - 28)
        let segment1 = (CGPoint(x: 50, y: 50), CGPoint(), CGPoint())
        let segment2 = (CGPoint(x: 100, y: -100), CGPoint(), CGPoint())
        let segments = [
            segment1,
            segment2
            ] as [(CGPoint,CGPoint,CGPoint)];
        let path = UIBezierPath()
        path.move(to: startPoint)
        var targetPoint = startPoint
        var controlPoint1 = startPoint
        var controlPoint2 = startPoint
        for (p, cp1, cp2) in segments{
            controlPoint1.x = targetPoint.x + cp1.x * scale
            controlPoint1.y = targetPoint.y + cp1.y * scale
            targetPoint.x += p.x * scale
            targetPoint.y += p.y * scale
            controlPoint2.x = targetPoint.x + cp2.x * scale
            controlPoint2.y = targetPoint.y + cp2.y * scale
            path.addCurve(to: targetPoint, controlPoint1: controlPoint1, controlPoint2: controlPoint2)
        }
        shapeLayer.path = path.cgPath
    }
    
    var notifficationsOverlayShapeLayer : CAShapeLayer?
    
    func playTickAnimation(){
        //init & fire the animation
        self.initNotifficationOverlayView()
        //prepare heart shape
        drawTick()
        self.notifficationsOverlayShapeLayer?.strokeEnd = 0
        self.notifficationsOverlayShapeLayer?.isHidden = false
        let animDuration : TimeInterval = 1.8
        UIView.animateKeyframes(withDuration: animDuration, delay: 0, options: [], animations: {
            /*UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.03){
                self.alpha = 1
            }*/
            UIView.addKeyframe(withRelativeStartTime: 0.03, relativeDuration: 0.67){
                let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
                pathAnimation.duration = 0.5 * animDuration
                pathAnimation.fromValue = NSNumber(value: 0 as Float)
                pathAnimation.toValue = NSNumber(value: 1 as Float)
                pathAnimation.fillMode = kCAFillModeForwards
                pathAnimation.isRemovedOnCompletion = false
                pathAnimation.timingFunction = CAMediaTimingFunction(controlPoints: 0.5, 0, 0.3, 1)
                self.notifficationsOverlayShapeLayer?.add(pathAnimation, forKey: "strokeEnd")
            }
            /*UIView.addKeyframe(withRelativeStartTime: 0.7, relativeDuration: 0.3){
                self.notifficationsOverlayShapeLayer?.opacity = 1
            }*/
        }){ c in
            self.initNotifficationOverlayView()
        }
    }
    
    func initNotifficationOverlayView(){
        guard self.notifficationsOverlayShapeLayer == nil else { return }
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = self.bounds
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.fillColor = nil
        shapeLayer.lineWidth = 6.7
        shapeLayer.lineCap = kCALineCapRound
        shapeLayer.lineJoin = kCALineJoinBevel
        self.layer.addSublayer(shapeLayer)
        self.notifficationsOverlayShapeLayer = shapeLayer
        
    }
}
