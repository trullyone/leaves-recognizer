//
//  TreeDetailsCardTableViewCell.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/8/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TreeDetailsCardTableViewCell: UITableViewCell {

    @IBOutlet weak var cardBackgroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cardBackgroundView.layer.cornerRadius = 9
        let cellLayer = self.layer
        cellLayer.shadowOffset = CGSize(width: 0, height: 3)
        cellLayer.shadowRadius = 5
        cellLayer.shadowOpacity = 0.2
    }

    

}
