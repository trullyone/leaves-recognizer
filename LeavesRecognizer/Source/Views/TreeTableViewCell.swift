//
//  TreeTableViewCell.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/7/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TreeTableViewCell: SelectableTableViewCell {

    @IBOutlet weak var imvTreeImage: UIImageView!
    @IBOutlet weak var lblTreeName: UILabel!
    @IBOutlet weak var lblTreeDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imvTreeImage.layer.cornerRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let actions : () -> () = {
            self.lblTreeName.textColor = selected ? UIColor.white : UIColor.black
        }
        switch animated {
        case true:
            UIView.animate(withDuration: 0.3, animations: actions)
        default:
            actions()
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        let actions : () -> () = {
            self.lblTreeName.textColor = highlighted ? UIColor.white : UIColor.black
        }
        switch highlighted {
        case true:
            UIView.animate(withDuration: 0.3, animations: actions)
        default:
            actions()
        }
    }
    
    func loadTreeInfo(treeInfo:TreeInfo){
        self.lblTreeName.text = treeInfo.title
        self.lblTreeDetails.text = treeInfo.subtitle
        self.imvTreeImage.image = UIImage(named: treeInfo.imageName)
    }
}
