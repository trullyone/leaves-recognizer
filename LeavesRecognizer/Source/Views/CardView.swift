//
//  CardView.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/13/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class CardView: UIView {

    @IBOutlet weak var cardBgView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cardBgView.layer.cornerRadius = 9
        let cardLayer = self.layer
        cardLayer.shadowOffset = CGSize(width: 0, height: 3)
        cardLayer.shadowRadius = 5
        cardLayer.shadowOpacity = 0.2
    }
}
