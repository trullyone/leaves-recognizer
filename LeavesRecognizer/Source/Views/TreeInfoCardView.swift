//
//  TreeInfoCardView.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/11/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TreeInfoCardView: CardView {

    var treeInfo : TreeInfo? {
        didSet{
            loadCurrentTreeInfo()
        }
    }
    
    @IBOutlet weak var imvTreeImage: UIImageView!
    @IBOutlet weak var lblTreeTitle: UILabel!
    @IBOutlet weak var lblTreeSubTitle: UILabel!
    @IBOutlet weak var textViewTreeDescription: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadCurrentTreeInfo()
    }
    
    func loadCurrentTreeInfo() {
        self.imvTreeImage?.image = self.treeInfo.flatMap({ UIImage(named: $0.imageName) })
        self.lblTreeTitle?.text = self.treeInfo?.title
        self.lblTreeSubTitle?.text = self.treeInfo?.subtitle
        self.textViewTreeDescription?.attributedText = loadContent(text: self.treeInfo?.content ?? "")
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    //#95989A
    func loadContent(text:String) -> NSAttributedString? {
        guard treeInfo != nil else {
            return nil
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        paragraphStyle.paragraphSpacing = 20
        return NSMutableAttributedString(string: text, attributes: [
            NSFontAttributeName : UIFont(name: "Helvetica Neue", size: 15.5)!,
            NSForegroundColorAttributeName : UIColor(red: 0x95/255.0, green: 0x98/255.0, blue: 0x9A/255.0, alpha: 1),
            NSParagraphStyleAttributeName : paragraphStyle])
    }
}
