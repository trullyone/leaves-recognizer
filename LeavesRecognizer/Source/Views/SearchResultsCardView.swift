//
//  SearchResultsCardView.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/8/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class SearchResultsCardView: CardView {
    
    weak var delegate : SearchResultsCardViewDelegate?
    var searchState : SearchState = .inProgress{
        didSet{
            updateUIByCurrentSearchState()
        }
    }
    
    @IBOutlet weak var imvImage : UIImageView!
    @IBOutlet weak var searchFailedView : UIView!
    @IBOutlet weak var searchingView : UIView!
    @IBOutlet weak var searchResultView : SearchResultView!
    
    var leafImage : UIImage? {
        didSet{
            self.imvImage.image = leafImage
        }
    }
    
    func beginSearch(){
        self.searchState = .inProgress
    }
    
    func endSearch(searchResult:(TreeInfo,Int)?){
        switch searchResult {
        case nil:
            self.searchState = .failed
            break
        default:
            loadSearchResult(searchResult: searchResult!)
            self.searchState = .success
            break
        }
    }
    
    func loadSearchResult(searchResult:(TreeInfo,Int)){
        self.searchResultView.loadSearchResult(searchResult: searchResult)
    }
    
    func updateUIByCurrentSearchState(){
        self.searchFailedView.isHidden = searchState != .failed
        self.searchingView.isHidden = searchState != .inProgress
        self.searchResultView.isHidden = searchState != .success
    }
    
    @IBAction func viewAllResultsUpInside(_ sender: Any) {
        self.delegate?.viewAllResultsUpInside(sender)
    }
    
    enum SearchState {
        case inProgress
        case success
        case failed
    }
}

protocol SearchResultsCardViewDelegate : NSObjectProtocol{
    func viewAllResultsUpInside(_ sender: Any)
}
