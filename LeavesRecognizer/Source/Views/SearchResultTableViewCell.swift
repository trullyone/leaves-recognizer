//
//  SearchResultTableViewCell.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/9/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: TreeTableViewCell {
    @IBOutlet weak var lblPercents: UILabel!
    
    func loadSearchResult(searchResult:(TreeInfo,Int)){
        self.lblPercents.text = String(searchResult.1) + "%"
        loadTreeInfo(treeInfo: searchResult.0)
    }
}
