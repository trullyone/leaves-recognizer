//
//  LightingModeButton.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/8/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class LightingModeButton: UIControl {

    
    @IBOutlet weak var imvFlashMode : UIImageView!
    
    var flashMode : AVCaptureFlashMode = .auto {
        didSet{
            updateUIByCurrentState()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addTarget(self, action: #selector(LightingModeButton.btnTouchUpInside), for: .touchUpInside)
    }
    
    func btnTouchUpInside(){
        switch flashMode {
        case .on:
            flashMode = .off
            break
        case .off:
            flashMode = .auto
            break
        case .auto:
            flashMode = .on
            break
        }
    }
    
    func updateUIByCurrentState(){
        switch flashMode {
        case .on:
            self.imvFlashMode.image = UIImage(named: "light_on")
            break
        case .off:
            self.imvFlashMode.image = UIImage(named: "light_off")
            break
        case .auto:
            self.imvFlashMode.image = UIImage(named: "light_auto")
            break
        }
    }
}
