//
//  SelectableTableViewCell.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/11/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class SelectableTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        let selectedView = UIView()
        selectedView.backgroundColor = UIColor(red: 0x52/255.0, green: 0x65/255.0, blue: 0x1E/255.0, alpha: 1)
        self.selectedBackgroundView = selectedView
    }
}
