//
//  TextTableViewCell.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/11/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TextTableViewCell: SelectableTableViewCell {

    @IBOutlet weak var lblText: UILabel!
    
   override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let actions : () -> () = {
            self.lblText.textColor = selected ? UIColor.white : UIColor.black
        }
        switch animated {
        case true:
            UIView.animate(withDuration: 0.3, animations: actions)
        default:
            actions()
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        let actions : () -> () = {
            self.lblText.textColor = highlighted ? UIColor.white : UIColor.black
        }
        switch highlighted {
        case true:
            UIView.animate(withDuration: 0.3, animations: actions)
        default:
            actions()
        }
    }
}
