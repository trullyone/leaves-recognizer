//
//  VideoPreviewView.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 7/24/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPreviewView: UIView {

    var videoPreviewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
    var session: AVCaptureSession? {
        get { return videoPreviewLayer.session }
        set {
            videoPreviewLayer.session = newValue
            self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        }
    }
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
    private var orientationMap: [UIDeviceOrientation : AVCaptureVideoOrientation] = [
        .portrait           : .portrait,
        .portraitUpsideDown : .portraitUpsideDown,
        .landscapeLeft      : .landscapeRight,
        .landscapeRight     : .landscapeLeft,
        ]
    func updateVideoOrientationForDeviceOrientation() {
        if let videoPreviewLayerConnection = videoPreviewLayer.connection {
            let deviceOrientation = UIDeviceOrientation.portrait //UIDevice.current.orientation
            guard let newVideoOrientation = orientationMap[deviceOrientation],
                deviceOrientation.isPortrait || deviceOrientation.isLandscape
                else { return }
            videoPreviewLayerConnection.videoOrientation = newVideoOrientation
        }
    }
    func focusAt(point:CGPoint, device: AVCaptureDevice) -> Bool {
        var success = true
        do{
            try device.lockForConfiguration()
            let poi = videoPreviewLayer.captureDevicePointOfInterest(for: point)
            device.focusPointOfInterest = poi
            device.focusMode = .autoFocus
            device.exposurePointOfInterest = poi
            device.exposureMode = .autoExpose
            device.unlockForConfiguration()
            
        }catch {
            print(error)
            success = false
        }
        return success
    }
}
