//
//  TLRSearchResultViewController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/8/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TLRSearchResultViewController: TLRCustomTitleViewController, SearchResultsCardViewDelegate {
    
    @IBOutlet weak var savedToLibraryAnimationView: SavedToLibraryAnimationView!
    weak var delegate : SearchResultViewControllerDelegate?
    
    var searchResult : [(TreeInfo,Int)]? {
        didSet{
            self.searchResultCardView.endSearch(searchResult: searchResult?.first)
            searchResult?.first.map({_,_ in 
                onEndCheckImageWithSuccess()
            })
        }
    }
    
    @IBOutlet weak var lcTreeInfoCardViewTopSpaceToSearchResultsView: NSLayoutConstraint!
    
    @IBOutlet weak var btnSaveImage: UIControl!
    @IBOutlet weak var searchResultCardView: SearchResultsCardView!
    @IBOutlet weak var treeInfoCardView: TreeInfoCardView!
    @IBOutlet weak var screenOverlayView: UIView!
    @IBOutlet weak var scrollViewContent: UIScrollView!
    
    let customVisionAIUrl = "https://southcentralus.api.cognitive.microsoft.com/customvision/v1.0/Prediction/455ac3a0-fd87-4d9f-9f56-76df02a5e408/image"
    let predictionKey = "16a3e0ea445e4d37b0ac492d457f569a"
    
    var imageIsFromPhotoGallery : Bool? {
        didSet{
            updateUIByImageIsFromGallery()
        }
    }
    var imageForRecognition : UIImage? {
        didSet{
            checkCurrentImage()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchResultCardView.delegate = self
        setTitle(text: "Search Results", icon: UIImage(named: "search icon"))
        let rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "help_icon"), style: .plain, target: self, action: #selector(self.helpRequested))
        self.navigationItem.setRightBarButton(rightBarButtonItem, animated: false)
        updateUIByImageIsFromGallery()
        checkCurrentImage()
    }
    
    func helpRequested(){
        presentHelpScreen(animated: true)
    }
    
    func presentHelpScreen(animated: Bool){
        let helpController = self.storyboard!.instantiateViewController(withIdentifier: "help") as! HelpScreenViewController
        self.present(helpController, animated: animated, completion: nil)
    }
    
    func checkCurrentImage(){
        guard self.isViewLoaded, let img = imageForRecognition else { return }
        onBeginCheckImage()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.navigationItem.hidesBackButton = true
        self.delegate?.willBeginCapturePhoto()
        self.searchResultCardView.beginSearch()
        self.searchResultCardView.leafImage = img
        let image = imageWithImage(image: img, scaledToSize: CGSize(width:400, height:400))
        let address = customVisionAIUrl
        let url = URL(string: address)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "post"
        request.addValue(predictionKey, forHTTPHeaderField: "Prediction-Key")
        request.addValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
        request.httpBody = UIImagePNGRepresentation(image)
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            defer { DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.navigationItem.hidesBackButton = false
                    self.delegate?.didEndCapturePhoto()
                }
            }
            switch (response, error) {
                case let (_,.some(err)):
                print(err.localizedDescription)
                break
                case let (r as HTTPURLResponse,.none) where r.statusCode == 200:
                    guard let data = data, let parse = try? JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary, let json = parse else { return }
                    let predictions = json["Predictions"] as? [AnyObject]
                    let tag_probability_pairs = predictions?.map({ ($0["Tag"] as? String, $0["Probability"] as? Float) })
                    let nonNullTag_probability_pairs = tag_probability_pairs?
                        .filter({ $0.0 != nil && $0.1 != nil })
                        .map({ ($0.0!, Int($0.1!.rounded() * 100) ) })
                        .filter({ $0.1 > 0 })
                        .sorted(by: { $0.1 > $1.1 }) ?? []
                    
                    DispatchQueue.main.async {
                        switch (nonNullTag_probability_pairs){
                            case let l where l.count > 0:
                                self.searchResult = l.map({ (tag, probability) in
                                    (TreeInfo.getTreeInfoByTag(tag:tag), probability) })
                            break
                            default:
                                //No results
                                self.searchResult = nil
                            break
                        }
                    }
                break
                default:
                //An error has ocurred
                self.searchResult = nil
                break;
            }
        })
        task.resume()
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        let imageSize = image.size
        let scaleToFillRatio = min(imageSize.width / newSize.width, imageSize.height / newSize.height)
        let calcuatedNewSize = CGSize(width: imageSize.width / scaleToFillRatio, height: imageSize.height / scaleToFillRatio)
        UIGraphicsBeginImageContextWithOptions(calcuatedNewSize, false, 0.0)
        image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: calcuatedNewSize.width, height: calcuatedNewSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func viewAllResultsUpInside(_ sender: Any) {
        let searchResultsController = self.storyboard!.instantiateViewController(withIdentifier: "searchResults") as! TLRSearchResultsTableViewController
        searchResultsController.searchResult = self.searchResult
        self.navigationController?.pushViewController(searchResultsController, animated: true)
    }
    
    func updateUIByImageIsFromGallery(){
        self.btnSaveImage?.isHidden = self.imageIsFromPhotoGallery ?? true
    }
    
    @IBAction func btnSaveImageTouchUpInside(_ sender: Any) {
        saveImage()
    }
    
    func saveImage(){
        self.imageIsFromPhotoGallery = nil
        DispatchQueue.global(qos: .background).async {
            UIImageWriteToSavedPhotosAlbum(self.imageForRecognition!, self, #selector(self.imageDidSaveToLibrary), nil)
        }
    }
    
    func imageDidSaveToLibrary(image: UIImage, error: NSError?, contextInfo: Any?){
        DispatchQueue.main.async {
            self.screenOverlayView?.isHidden = false
            self.savedToLibraryAnimationView?.playAnimation(onComplete:
                { c in self.screenOverlayView.isHidden = true }
            )
        }
    }
    
    func onBeginCheckImage(){
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.treeInfoCardView.isHidden = true
        self.scrollViewContent.scrollRectToVisible(self.scrollViewContent.bounds, animated: false)
        let w = self.scrollViewContent.contentSize.width
        let h = self.searchResultCardView.bounds.size.height + 30
        let size = CGSize(width: w, height: h)
        self.scrollViewContent.contentSize = size
    }
    
    func onEndCheckImageWithSuccess(){
        self.treeInfoCardView.isHidden = false
        self.treeInfoCardView.treeInfo = self.searchResult?.first?.0
        let w = self.scrollViewContent.contentSize.width
        let h = self.searchResultCardView.bounds.size.height + self.treeInfoCardView.bounds.size.height + 30
        let size = CGSize(width: w, height: h)
        self.scrollViewContent.contentSize = size
        
        self.lcTreeInfoCardViewTopSpaceToSearchResultsView.constant = 100
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.7) {
            self.lcTreeInfoCardViewTopSpaceToSearchResultsView.constant = 8
            self.view.layoutIfNeeded()
        }
    }
}

protocol SearchResultViewControllerDelegate : NSObjectProtocol {
    func willBeginCapturePhoto()
    func didEndCapturePhoto()
}
