//
//  CaptureImageViewController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 7/19/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class CaptureImageViewController: UIViewController, AVCapturePhotoCaptureDelegate {
    
    @IBOutlet weak var lightingModeButton: LightingModeButton!
    @IBOutlet weak var cameraPreviewViewOverlay: UIView!
    @IBOutlet weak var lcFocusPointViewLeadingSpaceToSuperView: NSLayoutConstraint!
    @IBOutlet weak var lcFocusPointViewTopSpaceToSuperView: NSLayoutConstraint!
    
    @IBOutlet weak var focusPointView: UIView!
    @IBOutlet weak var previewView: VideoPreviewView!
    
    var capturePhotoOutput : AVCapturePhotoOutput!
    var captureSession : AVCaptureSession!
    var isCaptureSessionConfigured = false // Instance proprerty on this view controller class
    var sessionQueue : DispatchQueue!
    weak var delegate : CaptureImageViewControllerDelegate?
    
    func defaultDevice() -> AVCaptureDevice {
        if let device = AVCaptureDevice.defaultDevice(withDeviceType: .builtInDualCamera,
                                                      mediaType: AVMediaTypeVideo,
                                                      position: .back) {
            return device // use dual camera on supported devices
        } else if let device = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera,
                                                             mediaType: AVMediaTypeVideo,
                                                             position: .back) {
            return device // use default back facing camera otherwise
        } else {
            fatalError("All supported devices are expected to have at least one of the queried capture devices.")
        }
    }
    
    func checkCameraAuthorization(_ completionHandler: @escaping ((_ authorized: Bool) -> Void)) {
        switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
        case .authorized:
            //The user has previously granted access to the camera.
            completionHandler(true)
            
        case .notDetermined:
            // The user has not yet been presented with the option to grant video access so request access.
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { success in
                completionHandler(success)
            })
            
        case .denied:
            // The user has previously denied access.
            completionHandler(false)
            
        case .restricted:
            // The user doesn't have the authority to request access e.g. parental restriction.
            completionHandler(false)
        }
    }
    
    func configureCaptureSession(_ completionHandler: ((_ success: Bool) -> Void)) {
        var success = false
        defer { completionHandler(success) } // Ensure all exit paths call completion handler.
        
        // Get video input for the default camera.
        let videoCaptureDevice = defaultDevice()
        guard let videoInput = try? AVCaptureDeviceInput(device: videoCaptureDevice) else {
            print("Unable to obtain video input for default camera.")
            return
        }
        
        // Create and configure the photo output.
        let capturePhotoOutput = AVCapturePhotoOutput()
        capturePhotoOutput.isHighResolutionCaptureEnabled = false
        capturePhotoOutput.isLivePhotoCaptureEnabled = false
        
        // Make sure inputs and output can be added to session.
        guard self.captureSession.canAddInput(videoInput) else { return }
        guard self.captureSession.canAddOutput(capturePhotoOutput) else { return }
        
        // Configure the session.
        self.captureSession.beginConfiguration()
        self.captureSession.sessionPreset = AVCaptureSessionPresetMedium
        self.captureSession.addInput(videoInput)
        self.captureSession.addOutput(capturePhotoOutput)
        self.captureSession.commitConfiguration()
        
        self.capturePhotoOutput = capturePhotoOutput
        
        success = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.sessionQueue = DispatchQueue(label: "sessionQueue")
        self.captureSession = AVCaptureSession()
        self.previewView.session = self.captureSession
        self.lightingModeButton.flashMode = .auto
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isCaptureSessionConfigured {
            if !self.captureSession.isRunning {
                self.captureSession.startRunning()
            }
        } else {
            // First time: request camera access, configure capture session and start it.
            self.checkCameraAuthorization({ authorized in
                guard authorized else {
                    print("Permission to use camera denied.")
                    return
                }
                self.sessionQueue.async {
                    self.configureCaptureSession({ success in
                        guard success else { return }
                        self.isCaptureSessionConfigured = true
                        self.captureSession.startRunning()
                        DispatchQueue.main.async {
                            self.previewView.updateVideoOrientationForDeviceOrientation()
                        }
                    })
                }
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if captureSession.isRunning {
            captureSession.stopRunning()
        }
    }
    
    @IBAction func btnCaptureTouchUpInside(_ sender: Any) {
        snapPhoto()
    }
    
    @IBAction func cameraPreviewViewTap(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: self.cameraPreviewViewOverlay)
        focusAt(point: point)
    }
    
    func snapPhoto() {
        guard let capturePhotoOutput = self.capturePhotoOutput else { return }
        let videoPreviewLayerOrientation = previewView.videoPreviewLayer.connection.videoOrientation
        self.sessionQueue.async {
            // Update the photo output's connection to match the video orientation of the video preview layer.
            if let photoOutputConnection = capturePhotoOutput.connection(withMediaType: AVMediaTypeVideo) {
                photoOutputConnection.videoOrientation = videoPreviewLayerOrientation
            }
            
            let photoSettings = AVCapturePhotoSettings()
            photoSettings.isAutoStillImageStabilizationEnabled = true
            photoSettings.isHighResolutionPhotoEnabled = false
            photoSettings.flashMode = self.lightingModeButton.flashMode
            
            /*let desiredPreviewPixelFormat = NSNumber(value: kCVPixelFormatType_32BGRA)
            if photoSettings.availablePreviewPhotoPixelFormatTypes.contains(desiredPreviewPixelFormat) {
                photoSettings.previewPhotoFormat = [
                    kCVPixelBufferPixelFormatTypeKey as String : desiredPreviewPixelFormat,
                    kCVPixelBufferWidthKey as String : 160,
                    kCVPixelBufferHeightKey as String : 160
                ]
            }*/
            
            capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self)
        }
    }
    
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?){
        delegate?.capture(captureOutput, didFinishProcessingPhotoSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer, error: error)
    }
    
    func focusAt(point: CGPoint){
        guard self.previewView.focusAt(point: point, device: defaultDevice()) else { return }
        self.showFocusPoint(at: point)
    }
    
    func showFocusPoint(at point:CGPoint){
        focusPointView.alpha = 1
        focusPointView.center = point
        UIView.animate(withDuration: 1) {
            self.focusPointView.alpha = 0
        }
    }
    
    
    @IBAction func btnCloseTouchUpInside(_ sender: Any) {
        self.delegate?.cancelPhotoCapturing()
    }
}

protocol CaptureImageViewControllerDelegate : NSObjectProtocol {
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, error: Error?)
    func cancelPhotoCapturing()
}
