//
//  TLRTreeDetailsViewController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/8/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TLRTreeDetailsViewController: TLRCustomTitleViewController {

    var treeInfo : TreeInfo? {
        didSet{
            loadCurrentTreeInfo()
        }
    }
    
    @IBOutlet weak var treeInfoCardView: TreeInfoCardView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(text: treeInfo?.title ?? "", icon: UIImage(named: "leaf icon"))
        loadCurrentTreeInfo()
    }

    func loadCurrentTreeInfo(){
        treeInfoCardView?.treeInfo = self.treeInfo
    }
    
}
