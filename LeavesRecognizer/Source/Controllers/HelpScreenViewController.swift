//
//  HelpScreenViewController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/14/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class HelpScreenViewController: UIViewController {

    var completion : (() -> Void)?
    
    @IBAction func btnCloseTouchUpInside(_ sender: Any) {
        dismiss(animated: true, completion: completion)
    }
}
