//
//  TLRExploreAllViewController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/7/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TLRTreeInfoListViewController: TLRCustomTitleViewController, UITableViewDataSource, UITableViewDelegate {

    var treeInfos : [TreeInfo]? {
        didSet{
            self.tableViewTrees?.reloadData()
        }
    }
    
    @IBOutlet weak var tableViewTrees: UITableView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableViewTrees.indexPathForSelectedRow.map { indexPath in
            self.tableViewTrees.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.treeInfos?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "treeCell") as! TreeTableViewCell
        let treeInfo = self.treeInfos![indexPath.row]
        cell.loadTreeInfo(treeInfo: treeInfo)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let treeDetailsController = self.storyboard!.instantiateViewController(withIdentifier: "tree_details") as! TLRTreeDetailsViewController
        let treeInfo = self.treeInfos![indexPath.row]
        treeDetailsController.treeInfo = treeInfo
        self.navigationController?.pushViewController(treeDetailsController, animated: true)
    }
}
