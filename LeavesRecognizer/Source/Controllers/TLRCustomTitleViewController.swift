//
//  CustomTitleViewController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/7/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TLRCustomTitleViewController: UIViewController {

    var icon : UIImage?{
        didSet{
            self.titleView?.icon = icon
        }
    }
    var navbarTitle : String?{
        didSet{
          self.titleView?.title = navbarTitle
        }
    }
    
    
    var titleView: TitleView! {
        didSet{
            self.navigationItem.titleView = titleView
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleView = Bundle.main.loadNibNamed("TitleView", owner: self, options: nil)?.first as? TitleView
        let ni = UIBarButtonItem.init(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = ni
        self.titleView?.title = navbarTitle
        self.titleView?.icon = icon
    }
    
    func setTitle(text: String, icon: UIImage?) {
        self.navbarTitle = text
        self.icon = icon
    }
}
