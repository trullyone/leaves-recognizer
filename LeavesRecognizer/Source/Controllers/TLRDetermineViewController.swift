//
//  TLRDetermineViewController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/7/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TLRDetermineViewController: TLRCustomTitleViewController, UITableViewDataSource, UITableViewDelegate {
    
    let criteriaNameCellId = "name"
    let criteriaValueCellId = "value"
    let checkResultsCellId = "check"
    
    private var cellIds : [String] = []
    private var criteria : Criteria?{
        didSet{
            tableViewPageLayout?.reloadData()
        }
    }
    
    @IBOutlet weak var tableViewPageLayout: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewPageLayout.rowHeight = UITableViewAutomaticDimension
        self.tableViewPageLayout.estimatedRowHeight = 5
        if navigationController?.viewControllers.count ?? 0 > 1 {
            setTitle(text: criteria?.name ?? "", icon: UIImage(named: "search icon"))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableViewPageLayout.indexPathForSelectedRow.map { indexPath in
            self.tableViewPageLayout.deselectRow(at: indexPath, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellId = cellIds[indexPath.row]
        switch cellId {
        case checkResultsCellId:
            checkOut()
            break
        default:
            let optionInfo = criteria?.possibleValues[indexPath.row - 1]
            select(criteria: optionInfo!)
            break
        }
    }
    
    func select(criteria:CriteriaOption){
        switch criteria.nextStep {
        case let criteriaResult as CriteriaResult:
            let treeInfo = TreeInfo.getTreeInfoById(id: criteriaResult.treeId)
            let controller = storyboard!.instantiateViewController(withIdentifier: "tree_details") as! TLRTreeDetailsViewController
            controller.treeInfo = treeInfo
            self.navigationController?.pushViewController(controller, animated: true)
            break
        case let nextCriteria as Criteria:
            let controller = storyboard!.instantiateViewController(withIdentifier: "determine") as! TLRDetermineViewController
            controller.setCriteria(criteria: nextCriteria, isInitial: false)
            self.navigationController?.pushViewController(controller, animated: true)
            break
        default: break
        }
    }
    
    func checkOut(){
        let controller = storyboard!.instantiateViewController(withIdentifier: "treeInfos") as!
        TLRTreeInfoListViewController
        controller.treeInfos = TreeInfo.getTreeInfosByCriteria(criteria: criteria!)
        controller.setTitle(text: "Search Results", icon: UIImage(named: "search icon"))
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellIds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = cellIds[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        switch cellId {
            case criteriaNameCellId:
            let criteriaNameCell = cell as! TextTableViewCell
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 5
            let text = NSMutableAttributedString(string: criteria?.name ?? "", attributes: [
                NSFontAttributeName : UIFont(name: "HelveticaNeue-Light", size: 22)!,
                NSForegroundColorAttributeName : UIColor(red: 0x65/255.0, green: 0x67/255.0, blue: 0x69/255.0, alpha: 1),
                NSParagraphStyleAttributeName : paragraphStyle])
            criteriaNameCell.lblText.attributedText = text
            break
            case criteriaValueCellId:
                let criteriaOptionCell = cell as! CriteriaOptionTableViewCell
                let optionInfo = criteria?.possibleValues[indexPath.row - 1]
                criteriaOptionCell.title = optionInfo?.title
                criteriaOptionCell.subTitle = optionInfo?.subTitle
            break
            case checkResultsCellId:
            //do nothing so far...
            break
            default: break
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let cellId = cellIds[indexPath.row]
        switch cellId {
        case criteriaNameCellId:
            return false
        default:
            return true
        }
    }
    
    func setCriteria(criteria:Criteria, isInitial:Bool){
        self.criteria = criteria
        self.cellIds = [ criteriaNameCellId ] + self.criteria!.possibleValues.map({ _ in criteriaValueCellId }) + (!isInitial ? [checkResultsCellId] : [])
    }
}
