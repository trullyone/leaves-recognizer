//
//  TLRMainScreenViewController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/7/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TLRMainScreenViewController: TLRCustomTitleViewController, UIScrollViewDelegate {
    
    var initialized : Bool = false {
        willSet{
            if !initialized {
                setSelectedPage(selectedPage: .ExploreAll, animated: false)
                self.pagesScrollView.contentSize = CGSize(width: pagesScrollView.bounds.width * 2, height: pagesScrollView.bounds.height)
                self.exploreAllController.view.frame = pagesScrollView.bounds
                self.determineController.view.frame =  pagesScrollView.bounds.offsetBy(dx: pagesScrollView.bounds.width, dy: 0)
            }
        }
    }
    
    @IBOutlet weak var pagesScrollView: UIScrollView!
    @IBOutlet weak var exploreAll: UIControl!
    @IBOutlet weak var determineTree: UIControl!
    @IBOutlet weak var lblExploreAll: UILabel!
    @IBOutlet weak var lblDetermineTree: UILabel!
    @IBOutlet weak var imvTabArrow: UIImageView!
    @IBOutlet weak var lcTabArrowLeadingSpaceToSuperView: NSLayoutConstraint!
    
    lazy var exploreAllController : TLRTreeInfoListViewController = { self.storyboard!.instantiateViewController(withIdentifier: "treeInfos") as! TLRTreeInfoListViewController}()
    lazy var determineController : TLRDetermineViewController = { self.storyboard!.instantiateViewController(withIdentifier: "determine") as! TLRDetermineViewController }()
    
    var scrollAnimationInProgress = false
    var scrollViewIsDragging = false
    var selectedPage : SelectedPage = .ExploreAll
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(text: "Tree Leaves Recognizer", icon: UIImage(named: "leaf icon"))
        setupChildControllers()
        self.pagesScrollView.delegate = self
    }
    
    func setupChildControllers(){
        self.addChildViewController(self.exploreAllController)
        self.pagesScrollView.addSubview(self.exploreAllController.view)
        self.exploreAllController.didMove(toParentViewController: self)
        self.exploreAllController.treeInfos = TreeInfo.getAllTreeInfo()
        
        self.addChildViewController(self.determineController)
        self.pagesScrollView.addSubview(self.determineController.view)
        self.determineController.didMove(toParentViewController: self)
        self.determineController.setCriteria(criteria: Criteria.rootCriteria!, isInitial: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.initialized = true
    }
    
    func setSelectedPage(selectedPage:SelectedPage, animated:Bool){
        guard !scrollAnimationInProgress && !scrollViewIsDragging else { return }
        if !animated {
            switch selectedPage {
            case .ExploreAll:
                self.lblExploreAll.alpha = 1
                self.lblDetermineTree.alpha = 0.5
                self.lcTabArrowLeadingSpaceToSuperView.constant = self.exploreAll.frame.midX - self.imvTabArrow.bounds.midX
                break
            case .Determine:
                self.lblExploreAll.alpha = 0.5
                self.lblDetermineTree.alpha = 1
                self.lcTabArrowLeadingSpaceToSuperView.constant = self.determineTree.frame.midX - self.imvTabArrow.bounds.midX
                break
            }
            self.view.layoutIfNeeded()
        }else{
            self.scrollAnimationInProgress = true
        }
        let screenWidth = self.pagesScrollView.bounds.width
        let screenHeight = self.pagesScrollView.bounds.height
        switch selectedPage {
        case .ExploreAll:
            self.pagesScrollView.scrollRectToVisible(CGRect.init(x: 0 * screenWidth, y: 0, width: screenWidth, height: screenHeight), animated: animated)
            break
        case .Determine:
            self.pagesScrollView.scrollRectToVisible(CGRect.init(x: 1 * screenWidth, y: 0, width: screenWidth, height: screenHeight), animated: animated)
            break
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let progress = scrollView.contentOffset.x / scrollView.bounds.width
        let arrowStartPoint = self.exploreAll.frame.midX - self.imvTabArrow.bounds.midX
        let arrowEndPoint = self.determineTree.frame.midX - self.imvTabArrow.bounds.midX
        self.lcTabArrowLeadingSpaceToSuperView.constant = arrowStartPoint + (arrowEndPoint - arrowStartPoint) * progress
        self.view.layoutIfNeeded()
        self.lblExploreAll.alpha = 1 - 0.5 * progress
        self.lblDetermineTree.alpha = 0.5 + 0.5 * progress
        self.selectedPage = progress <= 0.5 ? .ExploreAll : .Determine
        
        let pageOffset = scrollView.bounds.width.truncatingRemainder(dividingBy: scrollView.contentOffset.x)
        self.scrollAnimationInProgress = pageOffset > CGFloat(Float.ulpOfOne)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollAnimationInProgress = false
        self.scrollViewIsDragging = false
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageOffset = scrollView.bounds.width.truncatingRemainder(dividingBy: scrollView.contentOffset.x)
        self.scrollViewIsDragging = pageOffset > CGFloat(Float.ulpOfOne)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scrollViewIsDragging = true
    }
    
    @IBAction func exploreAllTouchUpInside(_ sender: Any) {
        guard self.selectedPage != .ExploreAll else { return }
        setSelectedPage(selectedPage: .ExploreAll, animated: true)
    }
    
    @IBAction func determineTreeTouchUpInside(_ sender: Any) {
        guard self.selectedPage != .Determine else { return }
        setSelectedPage(selectedPage: .Determine, animated: true)
    }
    
    enum SelectedPage {
        case ExploreAll
        case Determine
    }
}
