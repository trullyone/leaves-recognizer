//
//  TLRSearchResultsTableViewController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/9/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit

class TLRSearchResultsTableViewController: TLRCustomTitleViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableViewResults: UITableView!
    
    var searchResult : [(TreeInfo,Int)]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(text: "Search Results", icon: UIImage(named: "search icon"))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableViewResults.indexPathForSelectedRow.map { indexPath in
            self.tableViewResults.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResult?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultCell", for: indexPath) as! SearchResultTableViewCell
        let searchResult = self.searchResult![indexPath.row]
        cell.loadSearchResult(searchResult: searchResult)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let treeDetailsController = self.storyboard!.instantiateViewController(withIdentifier: "tree_details") as! TLRTreeDetailsViewController
        let treeInfo = self.searchResult![indexPath.row].0
        treeDetailsController.treeInfo = treeInfo
        self.navigationController?.pushViewController(treeDetailsController, animated: true)
    }
}
