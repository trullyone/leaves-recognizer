//
//  CaptureImageScreenOverlayController.swift
//  LeavesRecognizer
//
//  Created by Nickolay Kunev on 8/6/17.
//  Copyright © 2017 Divine Bulgaria Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class CaptureImageScreenOverlayController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CaptureImageViewControllerDelegate, SearchResultViewControllerDelegate {

    enum SelectImageState {
        case Open
        case Closed
    }
    
    let cameraIconSide: CGFloat = 72.0
    let imageIconSide: CGFloat = 40.0
    
    @IBOutlet weak var lcBtnSelectImageWidth: NSLayoutConstraint!
    @IBOutlet weak var lcBtnSelectImageHeight: NSLayoutConstraint!
    @IBOutlet weak var lcGalleryBottomSpaceToSuperView: NSLayoutConstraint!
    @IBOutlet weak var lcCameraBottomSpaceToSuperView: NSLayoutConstraint!
    @IBOutlet weak var lcCameraWidth: NSLayoutConstraint!
    @IBOutlet weak var lcCameraHeight: NSLayoutConstraint!
    @IBOutlet weak var lcGalleryWidth: NSLayoutConstraint!
    @IBOutlet weak var lcGalleryHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imvTransition: UIImageView!
    @IBOutlet weak var imvBtnEnabled: UIImageView!
    @IBOutlet weak var imvBtnClose: UIImageView!
    @IBOutlet weak var imvBtnDisabled: UIImageView!

    @IBOutlet weak var captureCloseBtn: UIControl!
    @IBOutlet weak var galleryBtn: UIControl!
    @IBOutlet weak var cameraBtn: UIControl!
    @IBOutlet weak var screenOverlay: UIView!
    @IBOutlet weak var contentView: UIView!
    
    var contentNavigationController: UINavigationController?
    
    var selectImageState = SelectImageState.Closed
    var captureEnabled = true
    
    let screenOverlayAlpha : CGFloat = 0.25
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        
        let setupLayer = { (layer:CALayer) in
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowRadius = 2
            layer.shadowOpacity = 0.3
        }
        
        setupLayer(self.captureCloseBtn.layer)
        setupLayer(self.galleryBtn.layer)
        setupLayer(self.cameraBtn.layer)
        
        setSelectImageState(selectImageState: SelectImageState.Closed, animated: false)
        setCaptureEnabled(enabled: true, animated: false)
        
        let navigationViewController = self.storyboard!.instantiateViewController(withIdentifier: "navigation_view_controller") as! UINavigationController
        self.addChildViewController(navigationViewController)
        navigationViewController.view.frame = self.contentView.bounds
        self.contentView.addSubview(navigationViewController.view)
        navigationViewController.didMove(toParentViewController: self)
        self.contentNavigationController = navigationViewController
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setCaptureEnabled(enabled: Bool, animated: Bool){
        self.captureEnabled = enabled
        switch animated {
        case true:
            setSelectImageState(selectImageState: SelectImageState.Closed, animated: false)
            UIView.animate(withDuration: 0.3, animations: {
                self.imvBtnEnabled.alpha = enabled ? 1 : 0
                self.imvBtnDisabled.alpha = !enabled ? 1 : 0
            })
            break
        default:
            self.imvBtnEnabled.alpha = enabled ? 1 : 0
            self.imvBtnDisabled.alpha = !enabled ? 1 : 0
            setSelectImageState(selectImageState: SelectImageState.Closed, animated: false)
            break
        }
    }
    
    func setSelectImageState(selectImageState:SelectImageState, animated: Bool){
        guard self.captureEnabled else { return }
        
        self.selectImageState = selectImageState
        switch (selectImageState, animated) {
        case (.Open, true):
            UIView.animateKeyframes(withDuration: 0.45, delay: 0, options: [], animations: {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.5, animations: {
                    self.imvBtnEnabled.alpha = 0
                    let val = CGFloat(25.0)
                    self.lcBtnSelectImageWidth.constant = val
                    self.lcBtnSelectImageHeight.constant = val
                    self.view.layoutIfNeeded()
                })
                UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.3, animations: {
                    self.imvBtnClose.alpha = 1
                    self.imvBtnClose.transform = CGAffineTransform(rotationAngle: 0)
                })
                UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.2, animations: {
                    self.lcGalleryBottomSpaceToSuperView.constant = 48
                    self.lcCameraBottomSpaceToSuperView.constant = 48
                    self.lcCameraWidth.constant = 17
                    self.lcCameraHeight.constant = 17
                    self.lcGalleryWidth.constant = 7
                    self.lcGalleryHeight.constant = 7
                    self.view.layoutIfNeeded()
                })
                UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.2, animations: {
                    self.lcCameraWidth.constant = 82 / 64 * self.cameraIconSide
                    self.lcCameraHeight.constant = 82 / 64 * self.cameraIconSide
                    self.view.layoutIfNeeded()
                })
                UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.1, animations: {
                    self.lcGalleryBottomSpaceToSuperView.constant = 119
                    self.lcCameraBottomSpaceToSuperView.constant = 46
                    self.view.layoutIfNeeded()
                })
                UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.3, animations: {
                    self.lcCameraWidth.constant = 64
                    self.lcCameraHeight.constant = 64
                    self.view.layoutIfNeeded()
                })
                UIView.addKeyframe(withRelativeStartTime: 0.7, relativeDuration: 0.15, animations: {
                    self.lcGalleryBottomSpaceToSuperView.constant = 134
                    self.lcGalleryWidth.constant = 48 / 36 * self.imageIconSide
                    self.lcGalleryHeight.constant = 48 / 36 * self.imageIconSide
                    self.view.layoutIfNeeded()
                })
                UIView.addKeyframe(withRelativeStartTime: 0.85, relativeDuration: 1, animations: {
                    self.lcCameraWidth.constant = self.cameraIconSide
                    self.lcCameraHeight.constant = self.cameraIconSide
                    self.lcGalleryWidth.constant = self.imageIconSide
                    self.lcGalleryHeight.constant = self.imageIconSide
                    self.view.layoutIfNeeded()
                })
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1, animations: {
                    self.screenOverlay.alpha = self.screenOverlayAlpha
                })
            }, completion: nil)
            break
        case (.Closed, true):
            UIView.animateKeyframes(withDuration: 0.25, delay: 0, options: [], animations: {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                    self.imvBtnClose.alpha = 0
                    self.imvBtnClose.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
                })
                UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7, animations: {
                    self.imvBtnEnabled.alpha = 1
                    let val = CGFloat(64.0)
                    self.lcBtnSelectImageWidth.constant = val
                    self.lcBtnSelectImageHeight.constant = val
                    self.view.layoutIfNeeded()
                })
                UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.4, animations: {
                    self.lcGalleryBottomSpaceToSuperView.constant = 5
                    self.lcCameraBottomSpaceToSuperView.constant = 5
                    self.lcCameraWidth.constant = 1
                    self.lcCameraHeight.constant = 1
                    self.lcGalleryWidth.constant = 1
                    self.lcGalleryHeight.constant = 1
                    self.view.layoutIfNeeded()
                })
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1, animations: {
                    self.screenOverlay.alpha = 0
                })
            }, completion: nil)
            break
        case (.Open, false):
            self.lcGalleryBottomSpaceToSuperView.constant = 134
            self.lcCameraBottomSpaceToSuperView.constant = 46
            self.lcCameraWidth.constant = 64
            self.lcCameraHeight.constant = 64
            self.lcGalleryWidth.constant = 36
            self.lcGalleryHeight.constant = 36
            self.screenOverlay.alpha = self.screenOverlayAlpha
            self.imvBtnClose.alpha = 1
            self.imvBtnClose.transform = CGAffineTransform(rotationAngle: CGFloat(0))
            self.view.layoutIfNeeded()
            break
        case (.Closed, false):
            self.lcGalleryBottomSpaceToSuperView.constant = 5
            self.lcCameraBottomSpaceToSuperView.constant = 5
            self.lcCameraWidth.constant = 1
            self.lcCameraHeight.constant = 1
            self.lcGalleryWidth.constant = 1
            self.lcGalleryHeight.constant = 1
            self.screenOverlay.alpha = 0
            self.imvBtnClose.alpha = 0
            self.imvBtnClose.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
            self.imvBtnEnabled.alpha = 1
            let val = CGFloat(64.0)
            self.lcBtnSelectImageWidth.constant = val
            self.lcBtnSelectImageHeight.constant = val
            self.view.layoutIfNeeded()
            break
        }
    }
    
    @IBAction func btnSelectImageTouchUpInside(_ sender: Any) {
        switch selectImageState {
        case .Open:
            setSelectImageState(selectImageState: .Closed, animated: true)
            break
        case .Closed:
            setSelectImageState(selectImageState: .Open, animated: true)
            break
        }
    }
    
    @IBAction func btnImageGalleryTouchUpInside(_ sender: Any) {
        setSelectImageState(selectImageState: .Closed, animated: true)
        
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .photoLibrary
        
        // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func btnCameraTouchUpInside(_ sender: Any) {
        setSelectImageState(selectImageState: .Closed, animated: true)
        let firstTime = UserDefaults.standard.bool(forKey: "firstTime")
        if !firstTime {
            UserDefaults.standard.set(true, forKey: "firstTime")
            presentHelpScreen(animated: true)
        }else{
            presentCapturePhotoScreen(animated: true)
        }
    }
    
    func presentCapturePhotoScreen(animated: Bool){
        let capturePhotoController = self.storyboard!.instantiateViewController(withIdentifier: "capturePhoto") as! CaptureImageViewController
        capturePhotoController.delegate = self
        self.present(capturePhotoController, animated: animated, completion: nil)
    }
    
    func presentHelpScreen(animated: Bool){
        let helpController = self.storyboard!.instantiateViewController(withIdentifier: "help") as! HelpScreenViewController
        helpController.completion = { self.presentCapturePhotoScreen(animated: false) }
        self.present(helpController, animated: animated, completion: nil)
    }
    
    @IBAction func screenOverlayToucUpInside(_ sender: Any) {
        setSelectImageState(selectImageState: .Closed, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        // Check the selected image.
        checkImageForTreeLeafRecogniztion(image: selectedImage, imageIsFromGallery: true)
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    func checkImageForTreeLeafRecogniztion(image:UIImage?, imageIsFromGallery: Bool){
        guard let image = image else { return }
        
        let setupSearchResultsController : (TLRSearchResultViewController) -> () = {
            $0.imageIsFromPhotoGallery = imageIsFromGallery
            $0.imageForRecognition = image
            $0.delegate = self
        }
        
        switch self.contentNavigationController?.topViewController {
        case let treeSearchResultsController as TLRSearchResultViewController:
            setupSearchResultsController(treeSearchResultsController)
            break
        default:
            let treeSearchResultsController = self.storyboard!.instantiateViewController(withIdentifier: "treeSearchResults") as! TLRSearchResultViewController
            setupSearchResultsController(treeSearchResultsController)
            self.contentNavigationController?.pushViewController(treeSearchResultsController, animated: false)
            break
        }
    }
    
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, error: Error?) {
        if let error = error {
            print(error.localizedDescription)
        }
        if let sampleBuffer = photoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: nil) {
            let image = UIImage(data: dataImage)
            checkImageForTreeLeafRecogniztion(image: image, imageIsFromGallery: false)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func cancelPhotoCapturing(){
        dismiss(animated: true, completion: nil)
    }
    
    func willBeginCapturePhoto(){
        setCaptureEnabled(enabled: false, animated: false)
    }
    
    func didEndCapturePhoto(){
        setCaptureEnabled(enabled: true, animated: false)
    }
}

